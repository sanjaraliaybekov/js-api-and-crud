//callback
// function firstFunction(a, callback) {
//     console.log('Birinchi sonimiz: ' + `${a}`);
//     callback();
// }
//
// firstFunction(5, function () {
//     console.log('Text')
// });
let myUsers = [];
let edited = -1;

async function getComments() {
    let res2 = await fetch('https://jsonplaceholder.typicode.com/users');
    let users = await res2.json();
    for (i = 0; i < users.length; i++) {
        myUsers.push(users[i])
    }
    console.log(myUsers);
    drawList();
}

getComments();

function drawList() {
    document.getElementById('comments').innerHTML = '';
    for (let i = 0; i < myUsers.length; i++) {
        document.getElementById('comments').innerHTML +=
            '<div class="col-md-4 mt-3">' +
            '<div class="card bg-dark text-white h-100">' +
            '<div class="card-header bg-dark">' + '<h3>' + 'User name: ' + myUsers[i].username + '</h3>' + '</div>' +
            '<div class="card-body">' +
            '<h4>' + 'User id: ' + myUsers[i].id + '</h4>' +
            '<h5>' + 'User email: ' + myUsers[i].email + '</h5>' +
            '<h5>' + 'User phone: ' + myUsers[i].phone + '</h5>' +
            '<h5>' + 'User website: ' + '<a href="https://' + myUsers[i].website + '">' + myUsers[i].website + '</a>' + '</h5>' +
            '</div>' +
            '<div class="card-footer justify-content-around d-flex">' +
            '<button type="button" data-target="#collapse" data-toggle="collapse" onclick="editUser(' + i + ')" class="btn btn-warning w-25">' + '<img src="img/pencil.svg" class="w-75">' + '</button>' +
            '<button type="button" onclick="deleteUser(' + i + ')" class="btn btn-danger w-25">' + '<img src="img/delete%20(2).svg" class="w-75">' + '</button>' +
            '</div>' +
            '</div>' +
            '</div>'

    }
}

function deleteUser(deleteIndex) {
    myUsers.splice(deleteIndex, 1);
    drawList();
}


function editUserAgain() {
    let userName = document.forms['myForm']['userName'].value;
    let userId = document.forms['myForm']['userId'].value;
    let userEmail = document.forms['myForm']['userEmail'].value;
    let userPhone = document.forms['myForm']['userPhone'].value;
    let userUrl = document.forms['myForm']['userUrl'].value;
    if (userName.trim().length > 0 && userId.trim().length > 0 && userEmail.trim().length > 0 && userPhone.trim().length > 0 && userUrl.trim().length > 0) {
        let newUser = {
            id: userId,
            username: userName,
            email: userEmail,
            phone: userPhone,
            website: userUrl
        };
        console.log(edited);
        if (edited >= 0) {
            myUsers[edited].id = document.forms['myForm']['userId'].value;
            myUsers[edited].username = document.forms['myForm']['userName'].value;
            myUsers[edited].email = document.forms['myForm']['userEmail'].value;
            myUsers[edited].phone = document.forms['myForm']['userPhone'].value;
            myUsers[edited].website = document.forms['myForm']['userUrl'].value;
        } else {
            myUsers.push(newUser);
        }
        drawList();
    } else {
        alert("Bo'sh joylarni to'ldiring!")
    }
    document.forms['myForm'].reset();
}

function editUser(editIndex) {
    document.forms['myForm']['userName'].value = myUsers[editIndex].username;
    document.forms['myForm']['userId'].value = myUsers[editIndex].id;
    document.forms['myForm']['userEmail'].value = myUsers[editIndex].email;
    document.forms['myForm']['userPhone'].value = myUsers[editIndex].phone;
    document.forms['myForm']['userUrl'].value = myUsers[editIndex].website;
    edited = editIndex;
    document.getElementById('userAdd').classList.add('d-none');
    document.getElementById('editButton').classList.remove('d-none');
}

function userAdd() {
    document.getElementById('userAdd').classList.add('d-none');
    document.getElementById('editButton').classList.remove('d-none');

}